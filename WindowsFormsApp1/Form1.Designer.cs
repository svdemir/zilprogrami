﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lblOgrenciMelodi = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbOgrenciMelodiler = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnOgrenciOynat = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnOgretmenOynat = new System.Windows.Forms.Button();
            this.cmbOgretmenMelodi = new System.Windows.Forms.ComboBox();
            this.lblOgretmenMelodi = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnOynatCikis = new System.Windows.Forms.Button();
            this.cmbCikisMelodiler = new System.Windows.Forms.ComboBox();
            this.lblCikisMelodi = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.btnAyarYukle = new System.Windows.Forms.Button();
            this.btnKaydet = new System.Windows.Forms.Button();
            this.uCPazartesi = new WindowsFormsApp1.UCgun();
            this.uCSali = new WindowsFormsApp1.UCgun();
            this.uCCarsamba = new WindowsFormsApp1.UCgun();
            this.uCPersembe = new WindowsFormsApp1.UCgun();
            this.uCCuma = new WindowsFormsApp1.UCgun();
            this.uCCumartesi = new WindowsFormsApp1.UCgun();
            this.uCPazar = new WindowsFormsApp1.UCgun();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(109, 16);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(213, 21);
            this.comboBox1.TabIndex = 1;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Location = new System.Drawing.Point(12, 61);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(434, 623);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Gainsboro;
            this.tabPage1.Controls.Add(this.uCPazartesi);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(426, 597);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "PAZARTESİ";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.Gainsboro;
            this.tabPage2.Controls.Add(this.uCSali);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(426, 597);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "SALI";
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.Gainsboro;
            this.tabPage3.Controls.Add(this.uCCarsamba);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(426, 597);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "ÇARŞAMBA";
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.Gainsboro;
            this.tabPage4.Controls.Add(this.uCPersembe);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(426, 597);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "PERŞEMBE";
            // 
            // tabPage5
            // 
            this.tabPage5.BackColor = System.Drawing.Color.Gainsboro;
            this.tabPage5.Controls.Add(this.uCCuma);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(426, 597);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "CUMA";
            // 
            // tabPage6
            // 
            this.tabPage6.BackColor = System.Drawing.Color.Gainsboro;
            this.tabPage6.Controls.Add(this.uCCumartesi);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(426, 597);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "CUMARTESİ";
            // 
            // tabPage7
            // 
            this.tabPage7.BackColor = System.Drawing.Color.Gainsboro;
            this.tabPage7.Controls.Add(this.uCPazar);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(426, 597);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "PAZAR";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 5000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // lblOgrenciMelodi
            // 
            this.lblOgrenciMelodi.AutoSize = true;
            this.lblOgrenciMelodi.Location = new System.Drawing.Point(140, 81);
            this.lblOgrenciMelodi.Name = "lblOgrenciMelodi";
            this.lblOgrenciMelodi.Size = new System.Drawing.Size(0, 13);
            this.lblOgrenciMelodi.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Ses Kartı Seçiniz";
            // 
            // cmbOgrenciMelodiler
            // 
            this.cmbOgrenciMelodiler.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbOgrenciMelodiler.FormattingEnabled = true;
            this.cmbOgrenciMelodiler.Location = new System.Drawing.Point(11, 28);
            this.cmbOgrenciMelodiler.Name = "cmbOgrenciMelodiler";
            this.cmbOgrenciMelodiler.Size = new System.Drawing.Size(164, 21);
            this.cmbOgrenciMelodiler.TabIndex = 5;
            this.cmbOgrenciMelodiler.SelectedIndexChanged += new System.EventHandler(this.cmbOgrenciMelodiler_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnOgrenciOynat);
            this.groupBox1.Controls.Add(this.cmbOgrenciMelodiler);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.lblOgrenciMelodi);
            this.groupBox1.Location = new System.Drawing.Point(463, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 123);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Öğrenci Zili";
            // 
            // btnOgrenciOynat
            // 
            this.btnOgrenciOynat.Image = global::zilProgrami.Properties.Resources.playStop;
            this.btnOgrenciOynat.Location = new System.Drawing.Point(11, 69);
            this.btnOgrenciOynat.Name = "btnOgrenciOynat";
            this.btnOgrenciOynat.Size = new System.Drawing.Size(60, 37);
            this.btnOgrenciOynat.TabIndex = 2;
            this.btnOgrenciOynat.UseVisualStyleBackColor = true;
            this.btnOgrenciOynat.Click += new System.EventHandler(this.btnOgrenciOynat_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(106, 81);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Süre";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.btnOgretmenOynat);
            this.groupBox2.Controls.Add(this.cmbOgretmenMelodi);
            this.groupBox2.Controls.Add(this.lblOgretmenMelodi);
            this.groupBox2.Location = new System.Drawing.Point(463, 152);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 123);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Öğretmen Zili";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(106, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Süre";
            // 
            // btnOgretmenOynat
            // 
            this.btnOgretmenOynat.Image = global::zilProgrami.Properties.Resources.playStop2;
            this.btnOgretmenOynat.Location = new System.Drawing.Point(11, 69);
            this.btnOgretmenOynat.Name = "btnOgretmenOynat";
            this.btnOgretmenOynat.Size = new System.Drawing.Size(60, 37);
            this.btnOgretmenOynat.TabIndex = 2;
            this.btnOgretmenOynat.UseVisualStyleBackColor = true;
            this.btnOgretmenOynat.Click += new System.EventHandler(this.btnOgretmenOynat_Click);
            // 
            // cmbOgretmenMelodi
            // 
            this.cmbOgretmenMelodi.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbOgretmenMelodi.FormattingEnabled = true;
            this.cmbOgretmenMelodi.Location = new System.Drawing.Point(11, 28);
            this.cmbOgretmenMelodi.Name = "cmbOgretmenMelodi";
            this.cmbOgretmenMelodi.Size = new System.Drawing.Size(164, 21);
            this.cmbOgretmenMelodi.TabIndex = 5;
            this.cmbOgretmenMelodi.SelectedIndexChanged += new System.EventHandler(this.cmbOgretmenMelodi_SelectedIndexChanged);
            // 
            // lblOgretmenMelodi
            // 
            this.lblOgretmenMelodi.AutoSize = true;
            this.lblOgretmenMelodi.Location = new System.Drawing.Point(140, 81);
            this.lblOgretmenMelodi.Name = "lblOgretmenMelodi";
            this.lblOgretmenMelodi.Size = new System.Drawing.Size(0, 13);
            this.lblOgretmenMelodi.TabIndex = 4;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.btnOynatCikis);
            this.groupBox3.Controls.Add(this.cmbCikisMelodiler);
            this.groupBox3.Controls.Add(this.lblCikisMelodi);
            this.groupBox3.Location = new System.Drawing.Point(463, 292);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(200, 123);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Çıkış Zili";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(106, 81);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Süre";
            // 
            // btnOynatCikis
            // 
            this.btnOynatCikis.Image = global::zilProgrami.Properties.Resources.playStop3;
            this.btnOynatCikis.Location = new System.Drawing.Point(11, 69);
            this.btnOynatCikis.Name = "btnOynatCikis";
            this.btnOynatCikis.Size = new System.Drawing.Size(60, 37);
            this.btnOynatCikis.TabIndex = 2;
            this.btnOynatCikis.UseVisualStyleBackColor = true;
            this.btnOynatCikis.Click += new System.EventHandler(this.btnOynatCikis_Click);
            // 
            // cmbCikisMelodiler
            // 
            this.cmbCikisMelodiler.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCikisMelodiler.FormattingEnabled = true;
            this.cmbCikisMelodiler.Location = new System.Drawing.Point(11, 28);
            this.cmbCikisMelodiler.Name = "cmbCikisMelodiler";
            this.cmbCikisMelodiler.Size = new System.Drawing.Size(164, 21);
            this.cmbCikisMelodiler.TabIndex = 5;
            this.cmbCikisMelodiler.SelectedIndexChanged += new System.EventHandler(this.cmbCikisMelodiler_SelectedIndexChanged);
            // 
            // lblCikisMelodi
            // 
            this.lblCikisMelodi.AutoSize = true;
            this.lblCikisMelodi.Location = new System.Drawing.Point(140, 81);
            this.lblCikisMelodi.Name = "lblCikisMelodi";
            this.lblCikisMelodi.Size = new System.Drawing.Size(0, 13);
            this.lblCikisMelodi.TabIndex = 4;
            // 
            // button1
            // 
            this.button1.Image = global::zilProgrami.Properties.Resources.layers__4_;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(501, 586);
            this.button1.Name = "button1";
            this.button1.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.button1.Size = new System.Drawing.Size(116, 47);
            this.button1.TabIndex = 2;
            this.button1.Text = "           Saatleri \r\n          Kopyala";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnAyarYukle
            // 
            this.btnAyarYukle.Image = ((System.Drawing.Image)(resources.GetObject("btnAyarYukle.Image")));
            this.btnAyarYukle.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAyarYukle.Location = new System.Drawing.Point(501, 514);
            this.btnAyarYukle.Name = "btnAyarYukle";
            this.btnAyarYukle.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btnAyarYukle.Size = new System.Drawing.Size(116, 47);
            this.btnAyarYukle.TabIndex = 2;
            this.btnAyarYukle.Text = "           Ayarları \r\n          Yükle";
            this.btnAyarYukle.UseVisualStyleBackColor = true;
            this.btnAyarYukle.Click += new System.EventHandler(this.btnAyarYukle_Click);
            // 
            // btnKaydet
            // 
            this.btnKaydet.Image = ((System.Drawing.Image)(resources.GetObject("btnKaydet.Image")));
            this.btnKaydet.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnKaydet.Location = new System.Drawing.Point(501, 449);
            this.btnKaydet.Name = "btnKaydet";
            this.btnKaydet.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btnKaydet.Size = new System.Drawing.Size(116, 47);
            this.btnKaydet.TabIndex = 2;
            this.btnKaydet.Text = "           Ayarları \r\n          Kaydet";
            this.btnKaydet.UseVisualStyleBackColor = true;
            this.btnKaydet.Click += new System.EventHandler(this.btnKaydet_Click);
            // 
            // uCPazartesi
            // 
            this.uCPazartesi.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.uCPazartesi.Location = new System.Drawing.Point(35, 7);
            this.uCPazartesi.Name = "uCPazartesi";
            this.uCPazartesi.Size = new System.Drawing.Size(348, 564);
            this.uCPazartesi.TabIndex = 1;
            // 
            // uCSali
            // 
            this.uCSali.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.uCSali.Location = new System.Drawing.Point(35, 7);
            this.uCSali.Name = "uCSali";
            this.uCSali.Size = new System.Drawing.Size(360, 582);
            this.uCSali.TabIndex = 0;
            // 
            // uCCarsamba
            // 
            this.uCCarsamba.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.uCCarsamba.Location = new System.Drawing.Point(35, 7);
            this.uCCarsamba.Name = "uCCarsamba";
            this.uCCarsamba.Size = new System.Drawing.Size(360, 582);
            this.uCCarsamba.TabIndex = 0;
            // 
            // uCPersembe
            // 
            this.uCPersembe.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.uCPersembe.Location = new System.Drawing.Point(35, 7);
            this.uCPersembe.Name = "uCPersembe";
            this.uCPersembe.Size = new System.Drawing.Size(360, 582);
            this.uCPersembe.TabIndex = 0;
            // 
            // uCCuma
            // 
            this.uCCuma.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.uCCuma.Location = new System.Drawing.Point(35, 7);
            this.uCCuma.Name = "uCCuma";
            this.uCCuma.Size = new System.Drawing.Size(360, 582);
            this.uCCuma.TabIndex = 0;
            // 
            // uCCumartesi
            // 
            this.uCCumartesi.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.uCCumartesi.Location = new System.Drawing.Point(35, 7);
            this.uCCumartesi.Name = "uCCumartesi";
            this.uCCumartesi.Size = new System.Drawing.Size(360, 582);
            this.uCCumartesi.TabIndex = 0;
            // 
            // uCPazar
            // 
            this.uCPazar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.uCPazar.Location = new System.Drawing.Point(35, 7);
            this.uCPazar.Name = "uCPazar";
            this.uCPazar.Size = new System.Drawing.Size(360, 582);
            this.uCPazar.TabIndex = 0;
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseClick);
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(683, 693);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnAyarYukle);
            this.Controls.Add(this.btnKaydet);
            this.Controls.Add(this.comboBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Zil Programı  v0.1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            this.tabPage7.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button btnOgrenciOynat;
        private System.Windows.Forms.Button btnKaydet;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private UCgun uCPazartesi;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TabPage tabPage7;
        private UCgun uCSali;
        private UCgun uCCarsamba;
        private UCgun uCPersembe;
        private UCgun uCCuma;
        private UCgun uCCumartesi;
        private UCgun uCPazar;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label lblOgrenciMelodi;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbOgrenciMelodiler;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnOgretmenOynat;
        private System.Windows.Forms.ComboBox cmbOgretmenMelodi;
        private System.Windows.Forms.Label lblOgretmenMelodi;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnOynatCikis;
        private System.Windows.Forms.ComboBox cmbCikisMelodiler;
        private System.Windows.Forms.Label lblCikisMelodi;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnAyarYukle;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
    }
}

