﻿namespace WindowsFormsApp1
{
    partial class UCgun
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpAna = new System.Windows.Forms.GroupBox();
            this.grbCikisZili = new System.Windows.Forms.GroupBox();
            this.maskedTextBox33 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox34 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox35 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox36 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox37 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox38 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox39 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox40 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox41 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox42 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox43 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox44 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox45 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox46 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox47 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox48 = new System.Windows.Forms.MaskedTextBox();
            this.grbOgretmenZili = new System.Windows.Forms.GroupBox();
            this.maskedTextBox17 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox18 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox19 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox20 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox21 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox22 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox23 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox24 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox25 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox26 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox27 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox28 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox29 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox30 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox31 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox32 = new System.Windows.Forms.MaskedTextBox();
            this.grbOgrenciZili = new System.Windows.Forms.GroupBox();
            this.maskedTextBox16 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox15 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox14 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox13 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox12 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox11 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox10 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox9 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox8 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox7 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox6 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox5 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox4 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox3 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox2 = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox1 = new System.Windows.Forms.MaskedTextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.grpAna.SuspendLayout();
            this.grbCikisZili.SuspendLayout();
            this.grbOgretmenZili.SuspendLayout();
            this.grbOgrenciZili.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpAna
            // 
            this.grpAna.Controls.Add(this.grbCikisZili);
            this.grpAna.Controls.Add(this.grbOgretmenZili);
            this.grpAna.Controls.Add(this.grbOgrenciZili);
            this.grpAna.Controls.Add(this.label16);
            this.grpAna.Controls.Add(this.label8);
            this.grpAna.Controls.Add(this.label15);
            this.grpAna.Controls.Add(this.label7);
            this.grpAna.Controls.Add(this.label14);
            this.grpAna.Controls.Add(this.label4);
            this.grpAna.Controls.Add(this.label13);
            this.grpAna.Controls.Add(this.label6);
            this.grpAna.Controls.Add(this.label12);
            this.grpAna.Controls.Add(this.label3);
            this.grpAna.Controls.Add(this.label11);
            this.grpAna.Controls.Add(this.label5);
            this.grpAna.Controls.Add(this.label10);
            this.grpAna.Controls.Add(this.label2);
            this.grpAna.Controls.Add(this.label9);
            this.grpAna.Controls.Add(this.label1);
            this.grpAna.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.grpAna.Location = new System.Drawing.Point(3, 3);
            this.grpAna.Name = "grpAna";
            this.grpAna.Size = new System.Drawing.Size(347, 571);
            this.grpAna.TabIndex = 0;
            this.grpAna.TabStop = false;
            // 
            // grbCikisZili
            // 
            this.grbCikisZili.Controls.Add(this.maskedTextBox33);
            this.grbCikisZili.Controls.Add(this.maskedTextBox34);
            this.grbCikisZili.Controls.Add(this.maskedTextBox35);
            this.grbCikisZili.Controls.Add(this.maskedTextBox36);
            this.grbCikisZili.Controls.Add(this.maskedTextBox37);
            this.grbCikisZili.Controls.Add(this.maskedTextBox38);
            this.grbCikisZili.Controls.Add(this.maskedTextBox39);
            this.grbCikisZili.Controls.Add(this.maskedTextBox40);
            this.grbCikisZili.Controls.Add(this.maskedTextBox41);
            this.grbCikisZili.Controls.Add(this.maskedTextBox42);
            this.grbCikisZili.Controls.Add(this.maskedTextBox43);
            this.grbCikisZili.Controls.Add(this.maskedTextBox44);
            this.grbCikisZili.Controls.Add(this.maskedTextBox45);
            this.grbCikisZili.Controls.Add(this.maskedTextBox46);
            this.grbCikisZili.Controls.Add(this.maskedTextBox47);
            this.grbCikisZili.Controls.Add(this.maskedTextBox48);
            this.grbCikisZili.Location = new System.Drawing.Point(261, 20);
            this.grbCikisZili.Name = "grbCikisZili";
            this.grbCikisZili.Size = new System.Drawing.Size(75, 542);
            this.grbCikisZili.TabIndex = 3;
            this.grbCikisZili.TabStop = false;
            this.grbCikisZili.Text = "Çıkış Zili";
            // 
            // maskedTextBox33
            // 
            this.maskedTextBox33.Location = new System.Drawing.Point(16, 508);
            this.maskedTextBox33.Mask = "00:00";
            this.maskedTextBox33.Name = "maskedTextBox33";
            this.maskedTextBox33.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox33.TabIndex = 15;
            this.maskedTextBox33.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox34
            // 
            this.maskedTextBox34.Location = new System.Drawing.Point(16, 476);
            this.maskedTextBox34.Mask = "00:00";
            this.maskedTextBox34.Name = "maskedTextBox34";
            this.maskedTextBox34.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox34.TabIndex = 14;
            this.maskedTextBox34.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox35
            // 
            this.maskedTextBox35.Location = new System.Drawing.Point(16, 444);
            this.maskedTextBox35.Mask = "00:00";
            this.maskedTextBox35.Name = "maskedTextBox35";
            this.maskedTextBox35.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox35.TabIndex = 13;
            this.maskedTextBox35.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox36
            // 
            this.maskedTextBox36.Location = new System.Drawing.Point(16, 412);
            this.maskedTextBox36.Mask = "00:00";
            this.maskedTextBox36.Name = "maskedTextBox36";
            this.maskedTextBox36.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox36.TabIndex = 12;
            this.maskedTextBox36.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox37
            // 
            this.maskedTextBox37.Location = new System.Drawing.Point(16, 380);
            this.maskedTextBox37.Mask = "00:00";
            this.maskedTextBox37.Name = "maskedTextBox37";
            this.maskedTextBox37.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox37.TabIndex = 11;
            this.maskedTextBox37.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox38
            // 
            this.maskedTextBox38.Location = new System.Drawing.Point(16, 348);
            this.maskedTextBox38.Mask = "00:00";
            this.maskedTextBox38.Name = "maskedTextBox38";
            this.maskedTextBox38.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox38.TabIndex = 10;
            this.maskedTextBox38.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox39
            // 
            this.maskedTextBox39.Location = new System.Drawing.Point(16, 316);
            this.maskedTextBox39.Mask = "00:00";
            this.maskedTextBox39.Name = "maskedTextBox39";
            this.maskedTextBox39.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox39.TabIndex = 9;
            this.maskedTextBox39.Text = "1730";
            this.maskedTextBox39.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox40
            // 
            this.maskedTextBox40.Location = new System.Drawing.Point(16, 284);
            this.maskedTextBox40.Mask = "00:00";
            this.maskedTextBox40.Name = "maskedTextBox40";
            this.maskedTextBox40.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox40.TabIndex = 8;
            this.maskedTextBox40.Text = "1640";
            this.maskedTextBox40.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox41
            // 
            this.maskedTextBox41.Location = new System.Drawing.Point(16, 252);
            this.maskedTextBox41.Mask = "00:00";
            this.maskedTextBox41.Name = "maskedTextBox41";
            this.maskedTextBox41.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox41.TabIndex = 7;
            this.maskedTextBox41.Text = "1550";
            this.maskedTextBox41.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox42
            // 
            this.maskedTextBox42.Location = new System.Drawing.Point(16, 220);
            this.maskedTextBox42.Mask = "00:00";
            this.maskedTextBox42.Name = "maskedTextBox42";
            this.maskedTextBox42.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox42.TabIndex = 6;
            this.maskedTextBox42.Text = "1500";
            this.maskedTextBox42.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox43
            // 
            this.maskedTextBox43.Location = new System.Drawing.Point(16, 188);
            this.maskedTextBox43.Mask = "00:00";
            this.maskedTextBox43.Name = "maskedTextBox43";
            this.maskedTextBox43.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox43.TabIndex = 5;
            this.maskedTextBox43.Text = "1410";
            this.maskedTextBox43.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox44
            // 
            this.maskedTextBox44.Location = new System.Drawing.Point(16, 156);
            this.maskedTextBox44.Mask = "00:00";
            this.maskedTextBox44.Name = "maskedTextBox44";
            this.maskedTextBox44.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox44.TabIndex = 4;
            this.maskedTextBox44.Text = "1230";
            this.maskedTextBox44.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox45
            // 
            this.maskedTextBox45.Location = new System.Drawing.Point(16, 124);
            this.maskedTextBox45.Mask = "00:00";
            this.maskedTextBox45.Name = "maskedTextBox45";
            this.maskedTextBox45.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox45.TabIndex = 3;
            this.maskedTextBox45.Text = "1140";
            this.maskedTextBox45.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox46
            // 
            this.maskedTextBox46.Location = new System.Drawing.Point(16, 92);
            this.maskedTextBox46.Mask = "00:00";
            this.maskedTextBox46.Name = "maskedTextBox46";
            this.maskedTextBox46.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox46.TabIndex = 2;
            this.maskedTextBox46.Text = "1050";
            this.maskedTextBox46.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox47
            // 
            this.maskedTextBox47.Location = new System.Drawing.Point(16, 60);
            this.maskedTextBox47.Mask = "00:00";
            this.maskedTextBox47.Name = "maskedTextBox47";
            this.maskedTextBox47.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox47.TabIndex = 1;
            this.maskedTextBox47.Text = "1000";
            this.maskedTextBox47.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox48
            // 
            this.maskedTextBox48.Location = new System.Drawing.Point(16, 28);
            this.maskedTextBox48.Mask = "00:00";
            this.maskedTextBox48.Name = "maskedTextBox48";
            this.maskedTextBox48.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox48.TabIndex = 0;
            this.maskedTextBox48.Text = "0910";
            this.maskedTextBox48.ValidatingType = typeof(System.DateTime);
            // 
            // grbOgretmenZili
            // 
            this.grbOgretmenZili.Controls.Add(this.maskedTextBox17);
            this.grbOgretmenZili.Controls.Add(this.maskedTextBox18);
            this.grbOgretmenZili.Controls.Add(this.maskedTextBox19);
            this.grbOgretmenZili.Controls.Add(this.maskedTextBox20);
            this.grbOgretmenZili.Controls.Add(this.maskedTextBox21);
            this.grbOgretmenZili.Controls.Add(this.maskedTextBox22);
            this.grbOgretmenZili.Controls.Add(this.maskedTextBox23);
            this.grbOgretmenZili.Controls.Add(this.maskedTextBox24);
            this.grbOgretmenZili.Controls.Add(this.maskedTextBox25);
            this.grbOgretmenZili.Controls.Add(this.maskedTextBox26);
            this.grbOgretmenZili.Controls.Add(this.maskedTextBox27);
            this.grbOgretmenZili.Controls.Add(this.maskedTextBox28);
            this.grbOgretmenZili.Controls.Add(this.maskedTextBox29);
            this.grbOgretmenZili.Controls.Add(this.maskedTextBox30);
            this.grbOgretmenZili.Controls.Add(this.maskedTextBox31);
            this.grbOgretmenZili.Controls.Add(this.maskedTextBox32);
            this.grbOgretmenZili.Location = new System.Drawing.Point(162, 20);
            this.grbOgretmenZili.Name = "grbOgretmenZili";
            this.grbOgretmenZili.Size = new System.Drawing.Size(83, 542);
            this.grbOgretmenZili.TabIndex = 2;
            this.grbOgretmenZili.TabStop = false;
            this.grbOgretmenZili.Text = "Öğretmen Zili";
            // 
            // maskedTextBox17
            // 
            this.maskedTextBox17.Location = new System.Drawing.Point(16, 508);
            this.maskedTextBox17.Mask = "00:00";
            this.maskedTextBox17.Name = "maskedTextBox17";
            this.maskedTextBox17.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox17.TabIndex = 15;
            this.maskedTextBox17.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox18
            // 
            this.maskedTextBox18.Location = new System.Drawing.Point(16, 476);
            this.maskedTextBox18.Mask = "00:00";
            this.maskedTextBox18.Name = "maskedTextBox18";
            this.maskedTextBox18.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox18.TabIndex = 14;
            this.maskedTextBox18.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox19
            // 
            this.maskedTextBox19.Location = new System.Drawing.Point(16, 444);
            this.maskedTextBox19.Mask = "00:00";
            this.maskedTextBox19.Name = "maskedTextBox19";
            this.maskedTextBox19.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox19.TabIndex = 13;
            this.maskedTextBox19.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox20
            // 
            this.maskedTextBox20.Location = new System.Drawing.Point(16, 412);
            this.maskedTextBox20.Mask = "00:00";
            this.maskedTextBox20.Name = "maskedTextBox20";
            this.maskedTextBox20.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox20.TabIndex = 12;
            this.maskedTextBox20.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox21
            // 
            this.maskedTextBox21.Location = new System.Drawing.Point(16, 380);
            this.maskedTextBox21.Mask = "00:00";
            this.maskedTextBox21.Name = "maskedTextBox21";
            this.maskedTextBox21.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox21.TabIndex = 11;
            this.maskedTextBox21.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox22
            // 
            this.maskedTextBox22.Location = new System.Drawing.Point(16, 348);
            this.maskedTextBox22.Mask = "00:00";
            this.maskedTextBox22.Name = "maskedTextBox22";
            this.maskedTextBox22.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox22.TabIndex = 10;
            this.maskedTextBox22.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox23
            // 
            this.maskedTextBox23.Location = new System.Drawing.Point(16, 316);
            this.maskedTextBox23.Mask = "00:00";
            this.maskedTextBox23.Name = "maskedTextBox23";
            this.maskedTextBox23.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox23.TabIndex = 9;
            this.maskedTextBox23.Text = "1650";
            this.maskedTextBox23.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox24
            // 
            this.maskedTextBox24.Location = new System.Drawing.Point(16, 284);
            this.maskedTextBox24.Mask = "00:00";
            this.maskedTextBox24.Name = "maskedTextBox24";
            this.maskedTextBox24.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox24.TabIndex = 8;
            this.maskedTextBox24.Text = "1600";
            this.maskedTextBox24.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox25
            // 
            this.maskedTextBox25.Location = new System.Drawing.Point(16, 252);
            this.maskedTextBox25.Mask = "00:00";
            this.maskedTextBox25.Name = "maskedTextBox25";
            this.maskedTextBox25.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox25.TabIndex = 7;
            this.maskedTextBox25.Text = "1510";
            this.maskedTextBox25.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox26
            // 
            this.maskedTextBox26.Location = new System.Drawing.Point(16, 220);
            this.maskedTextBox26.Mask = "00:00";
            this.maskedTextBox26.Name = "maskedTextBox26";
            this.maskedTextBox26.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox26.TabIndex = 6;
            this.maskedTextBox26.Text = "1420";
            this.maskedTextBox26.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox27
            // 
            this.maskedTextBox27.Location = new System.Drawing.Point(16, 188);
            this.maskedTextBox27.Mask = "00:00";
            this.maskedTextBox27.Name = "maskedTextBox27";
            this.maskedTextBox27.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox27.TabIndex = 5;
            this.maskedTextBox27.Text = "1330";
            this.maskedTextBox27.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox28
            // 
            this.maskedTextBox28.Location = new System.Drawing.Point(16, 156);
            this.maskedTextBox28.Mask = "00:00";
            this.maskedTextBox28.Name = "maskedTextBox28";
            this.maskedTextBox28.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox28.TabIndex = 4;
            this.maskedTextBox28.Text = "1150";
            this.maskedTextBox28.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox29
            // 
            this.maskedTextBox29.Location = new System.Drawing.Point(16, 124);
            this.maskedTextBox29.Mask = "00:00";
            this.maskedTextBox29.Name = "maskedTextBox29";
            this.maskedTextBox29.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox29.TabIndex = 3;
            this.maskedTextBox29.Text = "1100";
            this.maskedTextBox29.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox30
            // 
            this.maskedTextBox30.Location = new System.Drawing.Point(16, 92);
            this.maskedTextBox30.Mask = "00:00";
            this.maskedTextBox30.Name = "maskedTextBox30";
            this.maskedTextBox30.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox30.TabIndex = 2;
            this.maskedTextBox30.Text = "1010";
            this.maskedTextBox30.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox31
            // 
            this.maskedTextBox31.Location = new System.Drawing.Point(16, 60);
            this.maskedTextBox31.Mask = "00:00";
            this.maskedTextBox31.Name = "maskedTextBox31";
            this.maskedTextBox31.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox31.TabIndex = 1;
            this.maskedTextBox31.Text = "0920";
            this.maskedTextBox31.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox32
            // 
            this.maskedTextBox32.Location = new System.Drawing.Point(16, 28);
            this.maskedTextBox32.Mask = "00:00";
            this.maskedTextBox32.Name = "maskedTextBox32";
            this.maskedTextBox32.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox32.TabIndex = 0;
            this.maskedTextBox32.Text = "0830";
            this.maskedTextBox32.ValidatingType = typeof(System.DateTime);
            // 
            // grbOgrenciZili
            // 
            this.grbOgrenciZili.Controls.Add(this.maskedTextBox16);
            this.grbOgrenciZili.Controls.Add(this.maskedTextBox15);
            this.grbOgrenciZili.Controls.Add(this.maskedTextBox14);
            this.grbOgrenciZili.Controls.Add(this.maskedTextBox13);
            this.grbOgrenciZili.Controls.Add(this.maskedTextBox12);
            this.grbOgrenciZili.Controls.Add(this.maskedTextBox11);
            this.grbOgrenciZili.Controls.Add(this.maskedTextBox10);
            this.grbOgrenciZili.Controls.Add(this.maskedTextBox9);
            this.grbOgrenciZili.Controls.Add(this.maskedTextBox8);
            this.grbOgrenciZili.Controls.Add(this.maskedTextBox7);
            this.grbOgrenciZili.Controls.Add(this.maskedTextBox6);
            this.grbOgrenciZili.Controls.Add(this.maskedTextBox5);
            this.grbOgrenciZili.Controls.Add(this.maskedTextBox4);
            this.grbOgrenciZili.Controls.Add(this.maskedTextBox3);
            this.grbOgrenciZili.Controls.Add(this.maskedTextBox2);
            this.grbOgrenciZili.Controls.Add(this.maskedTextBox1);
            this.grbOgrenciZili.Location = new System.Drawing.Point(71, 20);
            this.grbOgrenciZili.Name = "grbOgrenciZili";
            this.grbOgrenciZili.Size = new System.Drawing.Size(75, 542);
            this.grbOgrenciZili.TabIndex = 1;
            this.grbOgrenciZili.TabStop = false;
            this.grbOgrenciZili.Text = "Öğrenci Zili";
            // 
            // maskedTextBox16
            // 
            this.maskedTextBox16.Location = new System.Drawing.Point(16, 508);
            this.maskedTextBox16.Mask = "00:00";
            this.maskedTextBox16.Name = "maskedTextBox16";
            this.maskedTextBox16.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox16.TabIndex = 15;
            this.maskedTextBox16.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox15
            // 
            this.maskedTextBox15.Location = new System.Drawing.Point(16, 476);
            this.maskedTextBox15.Mask = "00:00";
            this.maskedTextBox15.Name = "maskedTextBox15";
            this.maskedTextBox15.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox15.TabIndex = 14;
            this.maskedTextBox15.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox14
            // 
            this.maskedTextBox14.Location = new System.Drawing.Point(16, 444);
            this.maskedTextBox14.Mask = "00:00";
            this.maskedTextBox14.Name = "maskedTextBox14";
            this.maskedTextBox14.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox14.TabIndex = 13;
            this.maskedTextBox14.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox13
            // 
            this.maskedTextBox13.Location = new System.Drawing.Point(16, 412);
            this.maskedTextBox13.Mask = "00:00";
            this.maskedTextBox13.Name = "maskedTextBox13";
            this.maskedTextBox13.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox13.TabIndex = 12;
            this.maskedTextBox13.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox12
            // 
            this.maskedTextBox12.Location = new System.Drawing.Point(16, 380);
            this.maskedTextBox12.Mask = "00:00";
            this.maskedTextBox12.Name = "maskedTextBox12";
            this.maskedTextBox12.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox12.TabIndex = 11;
            this.maskedTextBox12.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox11
            // 
            this.maskedTextBox11.Location = new System.Drawing.Point(16, 348);
            this.maskedTextBox11.Mask = "00:00";
            this.maskedTextBox11.Name = "maskedTextBox11";
            this.maskedTextBox11.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox11.TabIndex = 10;
            this.maskedTextBox11.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox10
            // 
            this.maskedTextBox10.Location = new System.Drawing.Point(16, 316);
            this.maskedTextBox10.Mask = "00:00";
            this.maskedTextBox10.Name = "maskedTextBox10";
            this.maskedTextBox10.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox10.TabIndex = 9;
            this.maskedTextBox10.Text = "1647";
            this.maskedTextBox10.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox9
            // 
            this.maskedTextBox9.Location = new System.Drawing.Point(16, 284);
            this.maskedTextBox9.Mask = "00:00";
            this.maskedTextBox9.Name = "maskedTextBox9";
            this.maskedTextBox9.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox9.TabIndex = 8;
            this.maskedTextBox9.Text = "1557";
            this.maskedTextBox9.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox8
            // 
            this.maskedTextBox8.Location = new System.Drawing.Point(16, 252);
            this.maskedTextBox8.Mask = "00:00";
            this.maskedTextBox8.Name = "maskedTextBox8";
            this.maskedTextBox8.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox8.TabIndex = 7;
            this.maskedTextBox8.Text = "1507";
            this.maskedTextBox8.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox7
            // 
            this.maskedTextBox7.Location = new System.Drawing.Point(16, 220);
            this.maskedTextBox7.Mask = "00:00";
            this.maskedTextBox7.Name = "maskedTextBox7";
            this.maskedTextBox7.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox7.TabIndex = 6;
            this.maskedTextBox7.Text = "1417";
            this.maskedTextBox7.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox6
            // 
            this.maskedTextBox6.Location = new System.Drawing.Point(16, 188);
            this.maskedTextBox6.Mask = "00:00";
            this.maskedTextBox6.Name = "maskedTextBox6";
            this.maskedTextBox6.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox6.TabIndex = 5;
            this.maskedTextBox6.Text = "1327";
            this.maskedTextBox6.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox5
            // 
            this.maskedTextBox5.Location = new System.Drawing.Point(16, 156);
            this.maskedTextBox5.Mask = "00:00";
            this.maskedTextBox5.Name = "maskedTextBox5";
            this.maskedTextBox5.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox5.TabIndex = 4;
            this.maskedTextBox5.Text = "1147";
            this.maskedTextBox5.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox4
            // 
            this.maskedTextBox4.Location = new System.Drawing.Point(16, 124);
            this.maskedTextBox4.Mask = "00:00";
            this.maskedTextBox4.Name = "maskedTextBox4";
            this.maskedTextBox4.RejectInputOnFirstFailure = true;
            this.maskedTextBox4.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox4.TabIndex = 3;
            this.maskedTextBox4.Text = "1057";
            this.maskedTextBox4.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox3
            // 
            this.maskedTextBox3.Location = new System.Drawing.Point(16, 92);
            this.maskedTextBox3.Mask = "00:00";
            this.maskedTextBox3.Name = "maskedTextBox3";
            this.maskedTextBox3.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox3.TabIndex = 2;
            this.maskedTextBox3.Text = "1007";
            this.maskedTextBox3.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox2
            // 
            this.maskedTextBox2.Location = new System.Drawing.Point(16, 60);
            this.maskedTextBox2.Mask = "00:00";
            this.maskedTextBox2.Name = "maskedTextBox2";
            this.maskedTextBox2.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox2.TabIndex = 1;
            this.maskedTextBox2.Text = "0917";
            this.maskedTextBox2.ValidatingType = typeof(System.DateTime);
            // 
            // maskedTextBox1
            // 
            this.maskedTextBox1.Location = new System.Drawing.Point(16, 28);
            this.maskedTextBox1.Mask = "00:00";
            this.maskedTextBox1.Name = "maskedTextBox1";
            this.maskedTextBox1.Size = new System.Drawing.Size(40, 20);
            this.maskedTextBox1.TabIndex = 0;
            this.maskedTextBox1.Text = "0827";
            this.maskedTextBox1.ValidatingType = typeof(System.DateTime);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(10, 531);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(55, 13);
            this.label16.TabIndex = 1;
            this.label16.Text = "16. DERS";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(16, 275);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "8. DERS";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(10, 499);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(55, 13);
            this.label15.TabIndex = 1;
            this.label15.Text = "15. DERS";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 243);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "7. DERS";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(10, 403);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(55, 13);
            this.label14.TabIndex = 1;
            this.label14.Text = "12. DERS";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 147);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "4. DERS";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(10, 467);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(55, 13);
            this.label13.TabIndex = 1;
            this.label13.Text = "14. DERS";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 211);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "6. DERS";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(10, 371);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(55, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "11. DERS";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "3. DERS";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(10, 435);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(55, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "13. DERS";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 179);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "5. DERS";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(10, 339);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(55, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "10. DERS";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "2. DERS";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(16, 307);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "9. DERS";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "1. DERS";
            // 
            // UCgun
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.grpAna);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Name = "UCgun";
            this.Size = new System.Drawing.Size(359, 583);
            this.Load += new System.EventHandler(this.UCgun_Load);
            this.grpAna.ResumeLayout(false);
            this.grpAna.PerformLayout();
            this.grbCikisZili.ResumeLayout(false);
            this.grbCikisZili.PerformLayout();
            this.grbOgretmenZili.ResumeLayout(false);
            this.grbOgretmenZili.PerformLayout();
            this.grbOgrenciZili.ResumeLayout(false);
            this.grbOgrenciZili.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpAna;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox grbOgrenciZili;
        private System.Windows.Forms.MaskedTextBox maskedTextBox1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.MaskedTextBox maskedTextBox16;
        private System.Windows.Forms.MaskedTextBox maskedTextBox15;
        private System.Windows.Forms.MaskedTextBox maskedTextBox14;
        private System.Windows.Forms.MaskedTextBox maskedTextBox13;
        private System.Windows.Forms.MaskedTextBox maskedTextBox12;
        private System.Windows.Forms.MaskedTextBox maskedTextBox11;
        private System.Windows.Forms.MaskedTextBox maskedTextBox10;
        private System.Windows.Forms.MaskedTextBox maskedTextBox9;
        private System.Windows.Forms.MaskedTextBox maskedTextBox8;
        private System.Windows.Forms.MaskedTextBox maskedTextBox7;
        private System.Windows.Forms.MaskedTextBox maskedTextBox6;
        private System.Windows.Forms.MaskedTextBox maskedTextBox5;
        private System.Windows.Forms.MaskedTextBox maskedTextBox4;
        private System.Windows.Forms.MaskedTextBox maskedTextBox3;
        private System.Windows.Forms.MaskedTextBox maskedTextBox2;
        private System.Windows.Forms.GroupBox grbCikisZili;
        private System.Windows.Forms.MaskedTextBox maskedTextBox33;
        private System.Windows.Forms.MaskedTextBox maskedTextBox34;
        private System.Windows.Forms.MaskedTextBox maskedTextBox35;
        private System.Windows.Forms.MaskedTextBox maskedTextBox36;
        private System.Windows.Forms.MaskedTextBox maskedTextBox37;
        private System.Windows.Forms.MaskedTextBox maskedTextBox38;
        private System.Windows.Forms.MaskedTextBox maskedTextBox39;
        private System.Windows.Forms.MaskedTextBox maskedTextBox40;
        private System.Windows.Forms.MaskedTextBox maskedTextBox41;
        private System.Windows.Forms.MaskedTextBox maskedTextBox42;
        private System.Windows.Forms.MaskedTextBox maskedTextBox43;
        private System.Windows.Forms.MaskedTextBox maskedTextBox44;
        private System.Windows.Forms.MaskedTextBox maskedTextBox45;
        private System.Windows.Forms.MaskedTextBox maskedTextBox46;
        private System.Windows.Forms.MaskedTextBox maskedTextBox47;
        private System.Windows.Forms.MaskedTextBox maskedTextBox48;
        private System.Windows.Forms.GroupBox grbOgretmenZili;
        private System.Windows.Forms.MaskedTextBox maskedTextBox17;
        private System.Windows.Forms.MaskedTextBox maskedTextBox18;
        private System.Windows.Forms.MaskedTextBox maskedTextBox19;
        private System.Windows.Forms.MaskedTextBox maskedTextBox20;
        private System.Windows.Forms.MaskedTextBox maskedTextBox21;
        private System.Windows.Forms.MaskedTextBox maskedTextBox22;
        private System.Windows.Forms.MaskedTextBox maskedTextBox23;
        private System.Windows.Forms.MaskedTextBox maskedTextBox24;
        private System.Windows.Forms.MaskedTextBox maskedTextBox25;
        private System.Windows.Forms.MaskedTextBox maskedTextBox26;
        private System.Windows.Forms.MaskedTextBox maskedTextBox27;
        private System.Windows.Forms.MaskedTextBox maskedTextBox28;
        private System.Windows.Forms.MaskedTextBox maskedTextBox29;
        private System.Windows.Forms.MaskedTextBox maskedTextBox30;
        private System.Windows.Forms.MaskedTextBox maskedTextBox31;
        private System.Windows.Forms.MaskedTextBox maskedTextBox32;
    }
}
