﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class UCgun : UserControl
    {
        public UCgun()
        {
            InitializeComponent();
        }

        public List<string> OgrenciZilSaatleriniDön()
        {
            List<string> ogrenciZilleri = new List<string>();
            for (int i = 0; i < grbOgrenciZili.Controls.Count; i++)
            {
                ogrenciZilleri.Add(grbOgrenciZili.Controls[i].Text);
            }
            return ogrenciZilleri;
        }

        public List<string> OgretmenZilSaatleriniDön()
        {
            List<string> ogretmenZilleri = new List<string>();
            for (int i = 0; i < grbOgretmenZili.Controls.Count; i++)
            {
                ogretmenZilleri.Add(grbOgretmenZili.Controls[i].Text);
            }
            return ogretmenZilleri;
        }

        public List<string> CikisZilSaatleriniDön()
        {
            List<string> cikisZilleri = new List<string>();
            for (int i = 0; i < grbCikisZili.Controls.Count; i++)
            {
                cikisZilleri.Add(grbCikisZili.Controls[i].Text);
            }
            return cikisZilleri;
        }

        public void OgrenciZilSaatleriniEkle(List<string> ogrenciZilleri)
        {
            for (int i = 0; i < grbOgrenciZili.Controls.Count; i++)
            {
                if (i < ogrenciZilleri.Count)
                {
                    grbOgrenciZili.Controls[i].Text = ogrenciZilleri[i];
                }
            }
        }

        public void OgretmenZilSaatleriniEkle(List<string> ogretmenZilleri)
        {
            for (int i = 0; i < grbOgretmenZili.Controls.Count; i++)
            {
                if (i < ogretmenZilleri.Count)
                {
                    grbOgretmenZili.Controls[i].Text = ogretmenZilleri[i];
                }
            }
        }

        public void CikisZilSaatleriniEkle(List<string> cikisZilleri)
        {
            for (int i = 0; i < grbCikisZili.Controls.Count; i++)
            {
                if (i < cikisZilleri.Count)
                {
                    grbCikisZili.Controls[i].Text = cikisZilleri[i];
                }
            }
        }

        

        private void UCgun_Load(object sender, EventArgs e)
        {
            OgrenciZilSaatleriniDön();
        }
    }
}
