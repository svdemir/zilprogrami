﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        IWavePlayer waveOut;
        Guid myGuid;
        AudioFileReader audioFileReader;


        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                var sürücüler = DirectSoundOut.Devices;
                List<DirectSoundDeviceInfo> aygıtlar = sürücüler.ToList();
                comboBox1.DataSource = aygıtlar;
                comboBox1.DisplayMember = "Description";
                comboBox1.ValueMember = "Guid";
            }
            catch (Exception)
            {
                MessageBox.Show(" Tanımlı ses kartı bulunamadı. Program kapatılacak. ", " Ses Kartı", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(1);
            }


            try
            {
                string[] dosyalar = System.IO.Directory.GetFiles("melodiler");
                foreach (var dosya in dosyalar)
                {
                    var isimler = dosya.Split('\\');
                    var isim = isimler[isimler.Length - 1];
                    if (isim != "ayar.xml")
                    {
                        cmbOgrenciMelodiler.Items.Add(isim);
                        cmbOgretmenMelodi.Items.Add(isim);
                        cmbCikisMelodiler.Items.Add(isim);
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show(" Melodi klasörü bulunamadı. Lütfen melodiler klasörü oluşturunuz. Program kapatılacak. ", " Melodi Seçme", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(1);
            }


            if (cmbOgrenciMelodiler.Text != "")
            {
                MelodiSuresi(cmbOgrenciMelodiler, lblOgrenciMelodi);
            }

            if (cmbOgretmenMelodi.Text != "")
            {
                MelodiSuresi(cmbOgretmenMelodi, lblOgretmenMelodi);
            }

            if (cmbCikisMelodiler.Text != "")
            {
                MelodiSuresi(cmbCikisMelodiler, lblCikisMelodi);
            }

            btnAyarYukle.PerformClick();
        }


        private void CreateWaveOut()
        {
            myGuid = (Guid)comboBox1.SelectedValue;
            waveOut = new DirectSoundOut(myGuid, 300);
            waveOut.PlaybackStopped += OnPlaybackStopped;
        }

        void OnPlaybackStopped(object sender, StoppedEventArgs e)
        {
            if (e.Exception != null)
            {
                MessageBox.Show(e.Exception.Message, "Playback Device Error");
            }
            if (audioFileReader != null)
            {
                audioFileReader.Position = 0;
            }
        }

        private void Oynat()
        {
            if (waveOut != null)
            {
                if (waveOut.PlaybackState == PlaybackState.Playing)
                {
                    waveOut.Stop();
                    return;
                }
                else if (waveOut.PlaybackState == PlaybackState.Paused)
                {
                    waveOut.Play();
                    return;
                }
            }

            try
            {
                CreateWaveOut();
            }
            catch (Exception driverCreateException)
            {
                MessageBox.Show(String.Format("{0}", driverCreateException.Message));
                return;
            }



            try
            {
                waveOut.Init(audioFileReader);
            }
            catch (Exception initException)
            {
                MessageBox.Show(String.Format("{0}", initException.Message), "Error Initializing Output");
                return;
            }
            waveOut.Play();
        }


        #region Zil Saat İşlemleri

        string anlikSaat;
        string gun;

        List<string> ogrenciZilleri;
        List<string> ogretmenZilleri;
        List<string> cikisZilleri;

        private void ZilKontrol()
        {
            anlikSaat = DateTime.Now.ToShortTimeString();
            gun = CultureInfo.GetCultureInfo("tr-TR").DateTimeFormat.DayNames[(int)DateTime.Now.DayOfWeek];

            if (gun == "Pazartesi")
            {
                ZilSaatleriniGetir(uCPazartesi);
                ZilCal();
            }
            else if (gun == "Salı")
            {
                ZilSaatleriniGetir(uCSali);
                ZilCal();
            }
            else if (gun == "Çarşamba")
            {
                ZilSaatleriniGetir(uCCarsamba);
                ZilCal();
            }
            else if (gun == "Perşembe")
            {
                ZilSaatleriniGetir(uCPersembe);
                ZilCal();
            }
            else if (gun == "Cuma")
            {
                ZilSaatleriniGetir(uCCuma);
                ZilCal();
            }
            else if (gun == "Cumartesi")
            {
                ZilSaatleriniGetir(uCCumartesi);
                ZilCal();
            }
            else if (gun == "Pazar")
            {
                ZilSaatleriniGetir(uCPazar);
                ZilCal();
            }
        }

        private void ZilSaatleriniGetir(UCgun uCgun)
        {
            ogrenciZilleri = uCgun.OgrenciZilSaatleriniDön();
            ogretmenZilleri = uCgun.OgretmenZilSaatleriniDön();
            cikisZilleri = uCgun.CikisZilSaatleriniDön();
        }

        private void ZilCal()
        {
            for (int i = 0; i < ogrenciZilleri.Count; i++)
            {
                if (anlikSaat == ogrenciZilleri[i])
                {
                    OgrenciZiliCal();
                }

                if (anlikSaat == ogretmenZilleri[i])
                {
                    OgretmenZiliCal();
                }

                if (anlikSaat == cikisZilleri[i])
                {
                    CikisZiliCal();
                }
            }
        }

        private void OgrenciZiliCal()
        {
            if (cmbOgrenciMelodiler.Text != "")
            {
                audioFileReader = new AudioFileReader("melodiler\\" + cmbOgrenciMelodiler.Text);
                Oynat();

                //müzik bitiş işlemi...
                timer1.Interval = 75000;
            }
        }

        private void OgretmenZiliCal()
        {
            if (cmbOgretmenMelodi.Text != "")
            {
                audioFileReader = new AudioFileReader("melodiler\\" + cmbOgretmenMelodi.Text);
                Oynat();

                //müzik bitiş işlemi...
                timer1.Interval = 75000;
            }
        }

        private void CikisZiliCal()
        {
            if (cmbCikisMelodiler.Text != "")
            {
                audioFileReader = new AudioFileReader("melodiler\\" + cmbCikisMelodiler.Text);
                Oynat();

                //müzik bitiş işlemi...
                timer1.Interval = 75000;
            }
        }



        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Interval = 5000;

            ZilKontrol();
        }

        #endregion




        private void cmbOgrenciMelodiler_SelectedIndexChanged(object sender, EventArgs e)
        {
            MelodiSuresi(cmbOgrenciMelodiler, lblOgrenciMelodi);
        }

        private void cmbOgretmenMelodi_SelectedIndexChanged(object sender, EventArgs e)
        {
            MelodiSuresi(cmbOgretmenMelodi, lblOgretmenMelodi);
        }

        private void cmbCikisMelodiler_SelectedIndexChanged(object sender, EventArgs e)
        {
            MelodiSuresi(cmbCikisMelodiler, lblCikisMelodi);
        }

        private void MelodiSuresi(ComboBox cmbMelodi, Label lblSure)
        {
            try
            {
                AudioFileReader audioFileReader2 = new AudioFileReader("melodiler\\" + cmbMelodi.Text);    //müzik dosyalarının süresini hesaplamak için.
                lblSure.Text = String.Format("{0:00}:{1:00}", (int)audioFileReader2.TotalTime.TotalMinutes, audioFileReader2.TotalTime.Seconds);
            }
            catch (Exception)
            {
                MessageBox.Show(" Melodi işlemi hatası.", " Melodi Seçme", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnOgrenciOynat_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmbOgrenciMelodiler.Text != "")
                {
                    audioFileReader = new AudioFileReader("melodiler\\" + cmbOgrenciMelodiler.Text);
                    Oynat();
                }
            }
            catch (Exception)
            {
                MessageBox.Show(" Melodi işlemi hatası.", " Melodi Seçme", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnOgretmenOynat_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmbOgretmenMelodi.Text != "")
                {
                    audioFileReader = new AudioFileReader("melodiler\\" + cmbOgretmenMelodi.Text);
                    Oynat();
                }
            }
            catch (Exception)
            {
                MessageBox.Show(" Melodi işlemi hatası.", " Melodi Seçme", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnOynatCikis_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmbCikisMelodiler.Text != "")
                {
                    audioFileReader = new AudioFileReader("melodiler\\" + cmbCikisMelodiler.Text);
                    Oynat();
                }
            }
            catch (Exception)
            {
                MessageBox.Show(" Melodi işlemi hatası.", " Melodi Seçme", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnKaydet_Click(object sender, EventArgs e)
        {
            ayarlar ayarim = new ayarlar();

            ayarim.SesKarti = comboBox1.Text;

            ayarim.OgrenciZili = cmbOgrenciMelodiler.Text;
            ayarim.OgretmenZili = cmbOgretmenMelodi.Text;
            ayarim.CikisZili = cmbCikisMelodiler.Text;

            ayarim.PazartesiOgrenciZilleri = uCPazartesi.OgrenciZilSaatleriniDön();
            ayarim.PazartesiOgretmenilleri = uCPazartesi.OgretmenZilSaatleriniDön();
            ayarim.PazartesiCikisZilleri = uCPazartesi.CikisZilSaatleriniDön();

            ayarim.SaliOgrenciZilleri = uCSali.OgrenciZilSaatleriniDön();
            ayarim.SaliOgretmenilleri = uCSali.OgretmenZilSaatleriniDön();
            ayarim.SaliCikisZilleri = uCSali.CikisZilSaatleriniDön();

            ayarim.CarsambaOgrenciZilleri = uCCarsamba.OgrenciZilSaatleriniDön();
            ayarim.CarsambaOgretmenilleri = uCCarsamba.OgretmenZilSaatleriniDön();
            ayarim.CarsambaCikisZilleri = uCCarsamba.CikisZilSaatleriniDön();

            ayarim.PersembeOgrenciZilleri = uCPersembe.OgrenciZilSaatleriniDön();
            ayarim.PersembeOgretmenilleri = uCPersembe.OgretmenZilSaatleriniDön();
            ayarim.PersembeCikisZilleri = uCPersembe.CikisZilSaatleriniDön();

            ayarim.CumaOgrenciZilleri = uCCuma.OgrenciZilSaatleriniDön();
            ayarim.CumaOgretmenilleri = uCCuma.OgretmenZilSaatleriniDön();
            ayarim.CumaCikisZilleri = uCCuma.CikisZilSaatleriniDön();

            ayarim.CumartesiOgrenciZilleri = uCCumartesi.OgrenciZilSaatleriniDön();
            ayarim.CumartesiOgretmenilleri = uCCumartesi.OgretmenZilSaatleriniDön();
            ayarim.CumartesiCikisZilleri = uCCumartesi.CikisZilSaatleriniDön();

            ayarim.PazarOgrenciZilleri = uCPazar.OgrenciZilSaatleriniDön();
            ayarim.PazarOgretmenilleri = uCPazar.OgretmenZilSaatleriniDön();
            ayarim.PazarCikisZilleri = uCPazar.CikisZilSaatleriniDön();


            using (TextWriter writer = new StreamWriter(Application.StartupPath + @"\melodiler\ayar.xml"))
            {
                XmlSerializer xs = new XmlSerializer(typeof(ayarlar));
                xs.Serialize(writer, ayarim);
            }

            MessageBox.Show(" Ayarları kaydetme işlemi başarılı.", " Ayar Yükleme", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }


        private void btnAyarYukle_Click(object sender, EventArgs e)
        {
            ayarlar ayarim;

            try
            {
                using (TextReader reader = new StreamReader(Application.StartupPath + @"\melodiler\ayar.xml"))
                {
                    XmlSerializer xs = new XmlSerializer(typeof(ayarlar));
                    var data = xs.Deserialize(reader);
                    ayarim = (ayarlar)data;
                }
            }
            catch (Exception)
            {
                MessageBox.Show(" Ayarları yükleme işlemi hatası.", " Ayar Yükleme", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }


            comboBox1.Text = ayarim.SesKarti;
            cmbOgrenciMelodiler.Text = ayarim.OgrenciZili;
            cmbOgretmenMelodi.Text = ayarim.OgretmenZili;
            cmbCikisMelodiler.Text = ayarim.CikisZili;

            //zil saatleri

            uCPazartesi.OgrenciZilSaatleriniEkle(ayarim.PazartesiOgrenciZilleri);
            uCPazartesi.OgretmenZilSaatleriniEkle(ayarim.PazartesiOgretmenilleri);
            uCPazartesi.CikisZilSaatleriniEkle(ayarim.PazartesiCikisZilleri);

            uCSali.OgrenciZilSaatleriniEkle(ayarim.SaliOgrenciZilleri);
            uCSali.OgretmenZilSaatleriniEkle(ayarim.SaliOgretmenilleri);
            uCSali.CikisZilSaatleriniEkle(ayarim.SaliCikisZilleri);

            uCCarsamba.OgrenciZilSaatleriniEkle(ayarim.CarsambaOgrenciZilleri);
            uCCarsamba.OgretmenZilSaatleriniEkle(ayarim.CarsambaOgretmenilleri);
            uCCarsamba.CikisZilSaatleriniEkle(ayarim.CarsambaCikisZilleri);

            uCPersembe.OgrenciZilSaatleriniEkle(ayarim.PersembeOgrenciZilleri);
            uCPersembe.OgretmenZilSaatleriniEkle(ayarim.PersembeOgretmenilleri);
            uCPersembe.CikisZilSaatleriniEkle(ayarim.PersembeCikisZilleri);

            uCCuma.OgrenciZilSaatleriniEkle(ayarim.CumaOgrenciZilleri);
            uCCuma.OgretmenZilSaatleriniEkle(ayarim.CumaOgretmenilleri);
            uCCuma.CikisZilSaatleriniEkle(ayarim.CumaCikisZilleri);

            uCCumartesi.OgrenciZilSaatleriniEkle(ayarim.CumartesiOgrenciZilleri);
            uCCumartesi.OgretmenZilSaatleriniEkle(ayarim.CumartesiOgretmenilleri);
            uCCumartesi.CikisZilSaatleriniEkle(ayarim.CumartesiCikisZilleri);

            uCPazar.OgrenciZilSaatleriniEkle(ayarim.PazarOgrenciZilleri);
            uCPazar.OgretmenZilSaatleriniEkle(ayarim.PazarOgretmenilleri);
            uCPazar.CikisZilSaatleriniEkle(ayarim.PazarCikisZilleri);

            MessageBox.Show(" Ayarları yükleme işlemi başarılı.", " Ayar Yükleme", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<string> kopyaOgrenciZilleri = uCPazartesi.OgrenciZilSaatleriniDön();
            List<string> kopyaOgretmenZilleri = uCPazartesi.OgretmenZilSaatleriniDön();
            List<string> kopyaCikisZilleri = uCPazartesi.CikisZilSaatleriniDön();

            for (int i = 0; i < this.Controls["tabControl1"].Controls.Count; i++)
            {
                UCgun ha = (UCgun)this.Controls["tabControl1"].Controls[i].Controls[0];
                ha.OgrenciZilSaatleriniEkle(kopyaOgrenciZilleri);
                ha.OgretmenZilSaatleriniEkle(kopyaOgretmenZilleri);
                ha.CikisZilSaatleriniEkle(kopyaCikisZilleri);
            }

            MessageBox.Show(" Saatleri diğer günlere aktarma başarılı.", " Saat Aktarma", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }

        private bool kapatsorgu;
        DialogResult dr = System.Windows.Forms.DialogResult.No;

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!kapatsorgu)
            {
                dr = MessageBox.Show("  Program Kapatılacak Emin Misin? ", "   UYARI ", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                kapatsorgu = dr == System.Windows.Forms.DialogResult.Yes;
            }
            if (dr == System.Windows.Forms.DialogResult.Yes)
            {
                if (kapatsorgu)
                {
                    Application.Exit();
                }
                kapatsorgu = false;
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == this.WindowState)
            {
                this.Hide();
                notifyIcon1.Visible = true;
                notifyIcon1.Text = "Program Çalışıyor";
                notifyIcon1.BalloonTipTitle = "Program Çalışıyor";
                notifyIcon1.BalloonTipText = "Program sağ alt köşede konumlandı.";
                notifyIcon1.BalloonTipIcon = ToolTipIcon.Info;
                notifyIcon1.ShowBalloonTip(30000);
            }
            else if (FormWindowState.Normal == this.WindowState)
            {
                notifyIcon1.Visible = false;
            }
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
            notifyIcon1.Visible = false;
        }

        private void notifyIcon1_MouseClick(object sender, MouseEventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
            notifyIcon1.Visible = false;
        }
    }
}

