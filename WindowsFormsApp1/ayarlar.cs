﻿using System.Collections.Generic;

namespace WindowsFormsApp1
{
    public class ayarlar
    {
        public string SesKarti { get; set; }

        public string OgrenciZili { get; set; }

        public string OgretmenZili { get; set; }

        public string CikisZili { get; set; }

        public List<string> PazartesiOgrenciZilleri { get; set; }

        public List<string> PazartesiOgretmenilleri { get; set; }

        public List<string> PazartesiCikisZilleri { get; set; }

        public List<string> SaliOgrenciZilleri { get; set; }

        public List<string> SaliOgretmenilleri { get; set; }

        public List<string> SaliCikisZilleri { get; set; }

        public List<string> CarsambaOgrenciZilleri { get; set; }

        public List<string> CarsambaOgretmenilleri { get; set; }

        public List<string> CarsambaCikisZilleri { get; set; }

        public List<string> PersembeOgrenciZilleri { get; set; }

        public List<string> PersembeOgretmenilleri { get; set; }

        public List<string> PersembeCikisZilleri { get; set; }

        public List<string> CumaOgrenciZilleri { get; set; }

        public List<string> CumaOgretmenilleri { get; set; }

        public List<string> CumaCikisZilleri { get; set; }

        public List<string> CumartesiOgrenciZilleri { get; set; }

        public List<string> CumartesiOgretmenilleri { get; set; }

        public List<string> CumartesiCikisZilleri { get; set; }

        public List<string> PazarOgrenciZilleri { get; set; }

        public List<string> PazarOgretmenilleri { get; set; }

        public List<string> PazarCikisZilleri { get; set; }

    }
}
